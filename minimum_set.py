#!/usr/bin/env python3
import os

class Requirement:
	def __init__(self,rid):
		self.rid = rid
		self.adds = []
		self.constraints = []
		self.repeats = []
		self.canBeDecomposedInto = []
		self.parts = []
		self.refines = []
		self.contradicts = []
		self.alternatives = []
		self.sat = []
		self.satBy = []
		self.satisfiableByOther = False
	
	def __str__(self):
		ret= "\tR"+str(self.rid)+";\n"
		for r in self.adds:
			ret+= "\tR"+str(self.rid)+" -> R"+str(r.rid)+" [label=\"adds\"];\n"
		for r in self.constraints:
			ret+= "\tR"+str(self.rid)+" -> R"+str(r.rid)+" [label=\"constraints\"];\n"
		for r in self.repeats:
			ret+= "\tR"+str(self.rid)+" -> R"+str(r.rid)+" [label=\"repeats\"];\n"
		for r in self.canBeDecomposedInto:
			ret+= "\tR"+str(r.rid)+" -> R"+str(self.rid)+" [label=\"parts of\"];\n"
		for r in self.parts:
			ret+= "\tR"+str(r.rid)+" -> R"+str(self.rid)+" [label=\"parts of\"];\n"
		for r in self.refines:
			ret+= "\tR"+str(self.rid)+" -> R"+str(r.rid)+" [label=\"refines\"];\n"
		for r in self.contradicts:
			ret+= "\tR"+str(self.rid)+" -> R"+str(r.rid)+" [label=\"contradicts\"];\n"
		for r in self.alternatives:
			ret+= "\tR"+str(r.rid)+" -> R"+str(self.rid)+" [label=\"alternative to\"];\n"
		return ret
	
	def computeSat(self,reqs):
		for r in self.adds:
			if not r in self.sat:
				self.addSat(r)
		for r in self.repeats:
			if not r in self.sat:
				self.addSat(r)
			if not self in r.sat:
				r.addSat(self)
		for r in self.constraints:
			if not self in r.sat:
				r.addSat(self)
		decomposed = []
		for r in self.canBeDecomposedInto:
			if not r in self.sat:
				r.addSat(self)
			if not r in decomposed:
				decomposed.append(r)
		rid = ""
		for r in self.parts:
			if not r in self.sat:
				self.addSat(r)
		for r in self.refines:
			if not r in self.sat:
				self.addSat(r)
		for r in self.alternatives:
			if not self in r.sat:
				r.addSat(self)

	def addSat(self, r):
		if not r in self.sat:
			self.sat.append(r)
		if not self in r.satBy:
			r.satBy.append(self)


######################## Compute minimum set #########################

def naiveMinimumSat(reqs,verb=False):
	for r in reqs:
		if r.satBy != []:
			r.satisfiableByOther = True
	ret = []
	for r in reqs:
		if not r.satisfiableByOther:
			ret.append(r)
	return ret

def mininumSat(reqs,verb=False):
	ret = leafSat(reqs)
	eliminateUnnecessaryReqs(ret,reqs)
	return ret

def leafSat(reqs):
	for r in reqs:
		if not r.satisfiableByOther: # It has already been visited
			visit(r, [r])
	return [entry for entry in reqs if not entry.satisfiableByOther]

def visit(r, alreadyVisited):
	for s in r.sat:
		if not s in alreadyVisited:
			s.satisfiableByOther = True
			visit(s, alreadyVisited + [s])

def eliminateUnnecessaryReqs(ret,reqs):
	toRemove = []
	for r in ret:
		if len(r.sat) == 1:
			if r in r.sat[0].alternatives:
				for rs in r.sat[0].satBy:
					if rs.satisfiableByOther or not rs in toRemove:
						toRemove.append(r)
	for r in toRemove:
		if r in ret:
			ret.remove(r)

######################### Printing in Dot #########################

def dotSat(reqs):
	ret = "digraph RequirementsPropagation {\n"
	for r in reqs:
		ret += "\tR"+str(r.rid)+";\n"
		for s in r.sat:
			ret += "\tR"+str(r.rid)+" -> R"+str(s.rid)+" [label=\"satisfies\"];\n"
	for r in mininumSat(reqs):
		ret += "\tR" + str(r.rid)+ " [style=\"bold, filled\", fillcolor=\"gray\"];\n"
	return ret+"}"

def dot(reqs):
	ret = "digraph Requirements {\n"
	for r in reqs:
		ret += str(r)
	for r in mininumSat(reqs):
		ret += "\tR" + str(r.rid)+ " [style=\"bold, filled\", fillcolor=\"gray\"];\n"
	return ret+"}"

######################### Main #########################

r0 = Requirement(0)
r1 = Requirement(1)
r2 = Requirement(2)
r3 = Requirement(3)
r4 = Requirement(4)
r5 = Requirement(5)
r6 = Requirement(6)
r7 = Requirement(7)
r8 = Requirement(8)
r9 = Requirement(9)
r10 = Requirement(10)
r11 = Requirement(11)
r12 = Requirement(12)
r13 = Requirement(13)
r14 = Requirement(14)
r81 = Requirement(81)
r82 = Requirement(82)

reqs = [r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13, r14, r81, r82]

r0.adds.append(r2)
r0.adds.append(r3)
r2.adds.append(r3)
r2.adds.append(r4)
r1.adds.append(r4)
r1.canBeDecomposedInto.append(r7)
r1.canBeDecomposedInto.append(r8)
r0.constraints.append(r5)
r6.constraints.append(r2)
r5.refines.append(r2)
r81.refines.append(r8)
r82.refines.append(r8)
r8.repeats.append(r9)
r7.repeats.append(r14)
r12.adds.append(r2)
r11.adds.append(r10)
r10.adds.append(r13)
r13.adds.append(r11)

for r in reqs:
	r.computeSat(reqs)

print(dot(reqs))

mfile = open('requirementsDot.dot','w')
mfile.write(dot(reqs))
mfile.close()
os.system("dot -Tps requirementsDot.dot -o requirementsDot.pdf")

mfile = open('requirementsSat.dot','w')
mfile.write(dotSat(reqs))
mfile.close()

os.system("dot -Tps requirementsSat.dot -o requirementsSat.pdf")

print ("[")
for r in mininumSat(reqs, True):
	print (" - R"+str(r.rid))
print ("]")

